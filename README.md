# teach-unit-testing

Teach Unit Testing, Refactors, TDD

A project that Carles Mateo uses to teach Refactorings, Reliability of Code, Good Practices and Unit Testing to Friends and Colleagues.

Please, take in count that there are some bugs introduced on purpose, as I teach why it's wrong and how to correct them.

Lesson0 explained in two videos can be found at:
https://blog.carlesmateo.com/2020/03/31/lesson-0-learning-to-code-in-python-for-non-programmers/
