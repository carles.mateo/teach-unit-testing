from lesson1.csvoop import CSVOOP


class TestCSVOOP(object):

    a_test_print = [
                    ['Name', 'Country', 'Years in the Company'],
                    ['Michela', 'Italy, Montemarciano', '5'],
                    ['Sid', 'Catalonia', '2'],
                    ['Carles', 'Catalonia', '1'],
                    ['Jon', 'US', '1'],
                    ['Kris', 'Jersey', '1']
                   ]

    a_test_print2 = [
                     ['Name', 'Country', 'Years in the Company'],
                     ['Michela', 'Italy', '5'],
                     ['Sid', 'Catalonia', '2'],
                     ['Carles', 'Catalonia', '1'],
                     ['Jon', 'US', '1']
                    ]

    a_test_read_csv = [
                       ['Name', 'Country', 'Years in the Company'],
                       ['Michela', 'Italy, Montemarciano', '4'],
                       ['Sid', 'Catalonia', '1'],
                       ['Carles', 'Catalonia', '0'],
                       ['Jon', 'US', '0'],
                       ['Kris', 'Jersey', '0']
                      ]

    a_test_read_csv_floats = [
                               ['Name', 'Country', 'Years in the Company'],
                               ['Michela', 'Italy, Montemarciano', '4.9'],
                               ['Sid', 'Catalonia', '1.8'],
                               ['Carles', 'Catalonia', '0.9'],
                               ['Jon', 'US', '0.6'],
                               ['Kris', 'Jersey', '0.3']
                              ]

    a_test_update_year = [
                          ['Name', 'Country', 'Years in the Company'],
                          ['Michela', 'Italy, Montemarciano', '4'],
                          ['Sid', 'Catalonia', '1'],
                          ['Carles', 'Catalonia', '0'],
                          ['Jon', 'US', '0'],
                          ['Kris', 'Jersey', '0']
                         ]

    a_test_update_year_done = [
                               ['Name', 'Country', 'Years in the Company'],
                               ['Michela', 'Italy, Montemarciano', '5'],
                               ['Sid', 'Catalonia', '2'],
                               ['Carles', 'Catalonia', '1'],
                               ['Jon', 'US', '1'],
                               ['Kris', 'Jersey', '1']
                              ]

    a_test_update_year_three = [
                                ['Name', 'Country', 'Years in the Company'],
                                ['Michela', 'Italy, Montemarciano', '7'],
                                ['Sid', 'Catalonia', '4'],
                                ['Carles', 'Catalonia', '3'],
                                ['Jon', 'US', '3'],
                                ['Kris', 'Jersey', '3']
                               ]

    def test_read_csv_works(self):

        o_csvoop = CSVOOP()

        b_error, a_rows = o_csvoop.read_csv("csv1.csv")

        assert b_error is False
        assert a_rows == self.a_test_read_csv

    def test_read_csv_fails(self):

        o_csvoop = CSVOOP()

        b_error, a_rows = o_csvoop.read_csv("not-existing.csv")

        assert b_error is True
        assert a_rows == []

    def test_update_years_ok_for_1_year_from_real_file(self):

        o_csvoop = CSVOOP()

        b_error, a_rows_from_file = o_csvoop.read_csv("csv1.csv")
        i_rows, a_rows = o_csvoop.update_years(a_rows_from_file)

        assert i_rows == 6
        assert a_rows == self.a_test_update_year_done

    def test_update_years_ok_for_1_year(self):

        o_csvoop = CSVOOP()

        i_rows, a_rows = o_csvoop.update_years(self.a_test_update_year)

        assert i_rows == 6
        assert a_rows == self.a_test_update_year_done

    def test_update_years_ok_for_1_year_floats(self):

        o_csvoop = CSVOOP()

        i_rows, a_rows = o_csvoop.update_years(self.a_test_read_csv_floats)

        assert i_rows == 6
        assert a_rows == self.a_test_update_year_done

    def test_update_years_ok_for_3_years(self):

        o_csvoop = CSVOOP()

        i_rows, a_rows = o_csvoop.update_years(self.a_test_update_year, i_add=3)

        assert i_rows == 6
        assert a_rows == self.a_test_update_year_three

    def test_print_rows_works(self, capsys):

        o_csvoop = CSVOOP()

        o_csvoop.print_rows(self.a_test_print)

        o_captured = capsys.readouterr()

        assert o_captured.out == "['Name', 'Country', 'Years in the Company']" + "\n" + \
                                 "['Michela', 'Italy, Montemarciano', '5']" + "\n" + \
                                 "['Sid', 'Catalonia', '2']" + "\n" + \
                                 "['Carles', 'Catalonia', '1']" + "\n" + \
                                 "['Jon', 'US', '1']" + "\n" + \
                                 "['Kris', 'Jersey', '1']" + "\n"

        assert o_captured.err == ""

    def test_print_rows_limit_number_rows_ok(self, capsys):

        o_csvoop = CSVOOP()

        o_csvoop.print_rows(self.a_test_print, i_maxrows=2)

        o_captured = capsys.readouterr()

        assert o_captured.out == "['Name', 'Country', 'Years in the Company']" + "\n" + \
                                 "['Michela', 'Italy, Montemarciano', '5']" + "\n"

        assert o_captured.err == ""
