from lesson1.lib.fileutilites import FileUtilities


class TestFileUtilities(object):


    a_test_read_csv = [
                       ['Name', 'Country', 'Years in the Company'],
                       ['Michela', 'Italy, Montemarciano', '4'],
                       ['Sid', 'Catalonia', '1'],
                       ['Carles', 'Catalonia', '0'],
                       ['Jon', 'US', '0'],
                       ['Kris', 'Jersey', '0']
                      ]


    def test_read_csv_works(self):

        o_fileutilities = FileUtilities()

        b_error, a_rows = o_fileutilities.read_csv("csv1.csv")

        assert b_error is False
        assert a_rows == self.a_test_read_csv

    def test_read_csv_fails(self):

        o_fileutilities = FileUtilities()

        b_error, a_rows = o_fileutilities.read_csv("not-existing.csv")

        assert b_error is True
        assert a_rows == []

