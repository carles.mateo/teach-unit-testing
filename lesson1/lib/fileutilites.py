import csv


class FileUtilities():

    o_csv = csv

    def read_csv(self, s_file):
        a_rows = []
        b_error = False

        try:
            with open(s_file, newline='') as o_csv_file:
                o_reader = csv.reader(o_csv_file, delimiter=',')
                for a_row in o_reader:
                    a_rows.append(a_row)
        except:
            b_error = True

        return b_error, a_rows
