import csv

# With csv1.csv will work as intented
s_file = "csv1.csv"
# With csv2.csv will have problems with the CSV and converstion to int

with open(s_file, newline='') as o_csv_file:
    o_reader = csv.reader(o_csv_file, delimiter=',')
    i_row = 0
    for a_row in o_reader:
        i_row = i_row + 1
        # Just for Debug
        # print(a_row)
        if i_row > 1:
            i_years = int(a_row[2])
            i_years = i_years + 1
            a_row[2] = str(i_years)
        print(a_row)
