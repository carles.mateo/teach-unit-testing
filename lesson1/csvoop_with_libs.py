from lib.fileutilites import FileUtilities


class CSVOOPWithLibs():

    def update_years(self, a_rows, i_add=1):
        i_rows = 0
        a_output = []
        for a_row in a_rows:
            # Copy to a new array. We don't want to update to original mutable array
            a_row_new = []
            for s_value in a_row:
                a_row_new.append(s_value)

            if i_rows > 0:
                # Sanitize
                s_new_year = a_row_new[2].strip().strip('"')
                #a_row[2] = a_row[2].strip().strip('"')
                i_years = int(float(s_new_year))
                i_years = i_years + i_add
                a_row_new[2] = str(i_years)

            a_output.append(a_row_new)
            i_rows = i_rows + 1

        # Please explain on this returning/updating and do the proper way
        return i_rows, a_output

    def print_rows(self, a_rows, i_maxrows=1000):
        i_num_row = 0
        for a_row in a_rows:
            print(a_row)
            i_num_row = i_num_row + 1
            if i_num_row >= i_maxrows:
                break


def main():

    s_file = "csv1.csv"

    o_csvoop = CSVOOPWithLibs()
    o_fileutilities = FileUtilities()

    # First Block
    b_error, a_rows = o_fileutilities.read_csv(s_file)
    if b_error is True:
        print("An error has occurred parsing the CSV file: " + s_file)
        exit(1)

    # Second block
    i_rows, a_rows = o_csvoop.update_years(a_rows=a_rows, i_add=1)

    # Third block
    o_csvoop.print_rows(a_rows=a_rows)


if __name__ == "__main__":
    main()
