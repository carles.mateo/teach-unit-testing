import time


def get_small_tree():
    s_tree = "      *      \n" + \
             "     ***     \n" + \
             "    *****    \n" + \
             "   *******   \n" + \
             "  *********  \n" + \
             "      |      \n" + \
             "      |      \n" + "\n"
    return s_tree


def print_medium_tree():
    print("      *      ")
    print("     ***     ")
    print("    *****    ")
    print("   *******   ")
    print("  *********  ")
    print(" *********** ")
    print("*************")
    print("      |      ")
    print("      |      ")
    print("      |      ")
    print("      |      ")
    print("")


i_tree_counts = 1
i_tree_wanted = 5

while i_tree_counts <= i_tree_wanted:
    print("Iteration number:")
    print(i_tree_counts)
    if i_tree_counts > 3:
        print_medium_tree()
    else:
        s_small_tree = get_small_tree()
        print(s_small_tree)
    time.sleep(1)
    i_tree_counts = i_tree_counts + 1

